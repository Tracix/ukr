//
//  NetworkManager.swift
//  Tracix
//
//  Created by Aleksandr Lutenko on 11.11.2020.
//

import Foundation
import UIKit

class NetworkManager {
    
    var userIdentirier = UIDevice.current.identifierForVendor?.uuidString
    
    static let baseUrl: String = "http://tracix.abovebits.com/api/event"
    
    static let shared = NetworkManager()
    
    func sendProximityEvent() {
        let eventCode: String = "32"
        let body = ["eventCode": eventCode, "Lesson": "GET and POST"]
        post(body: body)
    }
    
    func sendRSSIMonitoring() {
        let eventCode: String = "33"
        let body = ["eventCode": eventCode, "Lesson": "GET and POST"]
        post(body: body)
    }
    
    func registration(phoneNumber: String, userName: String) {
        let wifiAddress = getIPAddress()
        let bluetooth = UIDevice.current.identifierForVendor?.uuidString ?? ""
        let dateFormatter = DateFormatter()
        let date = dateFormatter.string(from: Date())
        let eventCode: String = "31"
        let body: [String: Any] = ["eventCode": eventCode, "userName": userName, "phoneNumber": phoneNumber, "adressWiFi": wifiAddress, "adressBluetooth": bluetooth, "timeRegistration": date]
        post(body: body)
    }
    
    func sendLogRegistration() {
        let eventCode: String = "1"
        let body = ["eventCode": eventCode, "Lesson": "GET and POST"]
        post(body: body)
    }
    
    func sendLogStartProximityEvent() {
        let eventCode: String = "2"
        let body = ["eventCode": eventCode, "Lesson": "GET and POST"]
        post(body: body)
    }
    
    func sendLogStopProximityEvent() {
        let eventCode: String = "3"
        let body = ["eventCode": eventCode, "Lesson": "GET and POST"]
        post(body: body)
    }
    
    func sendLogDeregistration() {
        let eventCode: String = "4"
        let body = ["eventCode": eventCode, "Lesson": "GET and POST"]
        post(body: body)
    }
    
    private func post(body: [String: Any]) {
        guard let url = URL(string: NetworkManager.baseUrl) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        guard let httpBody = try? JSONSerialization.data(withJSONObject: body, options: []) else { return }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            guard let response = response, let data = data else { return }
            print(response)
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
            } catch {
                print(error)
            }
        } .resume()
    }
    
    func registration(json: Any) {
        
    }
}
