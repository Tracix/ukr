//
//  LocationManager.swift
//  Tracix
//
//  Created by Aleksandr Lutenko on 26.11.2020.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    private var locationManager = CLLocationManager()
    var peripheralManager = BluetoothPeripheralManager()
    var centralManager = BluetoothCentralService()
    private var rangeForBeacon: UUID?
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        rangeForBeacon = UUID(uuidString: Constants.SERVICE_UUID.uuidString)
    }
    
    func start() {
        locationManager.startUpdatingLocation()
        centralManager.starScan()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("locations \(locations)")
    }
    
//    func start() {
//        print("startUpdatingLocation")
//        locationManager.startUpdatingLocation()
//
//        // Start beacon ranging
//        guard let beaconUUID = rangeForBeacon else {
//            return
//        }
//        if #available(iOS 13.0, *) {
//            locationManager.startRangingBeacons(satisfying: CLBeaconIdentityConstraint(uuid: beaconUUID))
//        } else {
//            let beaconRegion = CLBeaconRegion(proximityUUID: beaconUUID, identifier: beaconUUID.uuidString)
//            locationManager.startRangingBeacons(in: beaconRegion)
//        }
//    }
}
