//
//  Constants.swift
//  Tracix
//
//  Created by Aleksandr Lutenko on 29.10.2020.
//

import Foundation
import CoreBluetooth

struct Constants {
    static let SERVICE_UUID = CBUUID(string: "4DF91029-B356-463E-9F48-BAB077BF3EF5")
    static let nameCharatteristic_UUID = CBUUID(string: "3B66D024-2336-4F22-A980-8095F4898C42")
    static let writebleCharatteristic_UUID = CBUUID(string: "3B66D024-2336-4F22-A980-8095F4898C43")
    static let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    static let rssiUpperBound = -52
    static let rssiLowerBound : Int = -63
    static let rssiThreshold = -69
    static let secondsBeforeProximityEventExpires = 5.0
    static let maxDurationInSeconds = 31.0
}
