//
//  StatisticsLog.swift
//
//  Copyright 2020 VMware, Inc.
//  SPDX-License-Identifier: MIT
//

import Foundation

/// CSV contact log for post event analysis and visualisation
class StatisticsLog: NSObject, SensorDelegate {
    private let payloadData: PayloadData
    private var identifierToPayload: [TargetIdentifier:String] = [:]
    private var payloadToTime: [String:Date] = [:]
    private var payloadToSample: [String:Sample] = [:]
    
    init(filename: String, payloadData: PayloadData) {
        self.payloadData = payloadData
    }
    
    
    private func add(identifier: TargetIdentifier) {
        guard let payload = identifierToPayload[identifier] else {
            return
        }
        add(payload: payload)
    }

    private func add(payload: String) {
        guard let time = payloadToTime[payload], let sample = payloadToSample[payload] else {
            payloadToTime[payload] = Date()
            payloadToSample[payload] = Sample()
            return
        }
        let now = Date()
        payloadToTime[payload] = now
        sample.add(Double(now.timeIntervalSince(time)))
    }

    // MARK:- SensorDelegate
    
    func sensor(_ sensor: SensorType, didRead: PayloadData, fromTarget: TargetIdentifier) {
        identifierToPayload[fromTarget] = didRead.shortName
        add(identifier: fromTarget)
    }
    
    func sensor(_ sensor: SensorType, didMeasure: Proximity, fromTarget: TargetIdentifier) {
        add(identifier: fromTarget)
    }
    
    func sensor(_ sensor: SensorType, didShare: [PayloadData], fromTarget: TargetIdentifier) {
        didShare.forEach() { payloadData in
            add(payload: payloadData.shortName)
        }
    }
}
