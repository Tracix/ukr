//
//  DetectionLog.swift
//
//  Copyright 2020 VMware, Inc.
//  SPDX-License-Identifier: MIT
//

import Foundation
import UIKit

/// CSV contact log for post event analysis and visualisation
class DetectionLog: NSObject, SensorDelegate {
    private let logger = ConcreteSensorLogger(subsystem: "Sensor", category: "Data.DetectionLog")
    private let payloadData: PayloadData
    private let deviceName = UIDevice.current.name
    private let deviceOS = UIDevice.current.systemVersion
    private var payloads: Set<String> = []
    private let queue = DispatchQueue(label: "Sensor.Data.DetectionLog.Queue")
    
    init(filename: String, payloadData: PayloadData) {
        self.payloadData = payloadData
        super.init()
    }
    
    // MARK:- SensorDelegate
    
    func sensor(_ sensor: SensorType, didRead: PayloadData, fromTarget: TargetIdentifier) {
        queue.async {
            if self.payloads.insert(didRead.shortName).inserted {
                self.logger.debug("didRead (payload=\(didRead.shortName))")
            }
        }
    }
    
    func sensor(_ sensor: SensorType, didShare: [PayloadData], fromTarget: TargetIdentifier) {
        didShare.forEach() { payloadData in
            queue.async {
                if self.payloads.insert(payloadData.shortName).inserted {
                    self.logger.debug("didShare (payload=\(payloadData.shortName))")
                }
            }
        }
    }
}
