//
//  FirstVC.swift
//  Tracix
//
//  Created by Aleksandr Lutenko on 06.11.2020.
//

import Foundation
import UIKit

class FirstVC: UIViewController {
    
    @IBOutlet weak var number: UITextField!
    
    @IBAction func next() {
        if let numberString = number.text {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            viewController.deviceIdentifier = numberString
            getCurrentDeviceName(identifire: numberString)
            present(viewController, animated: true)
        }
    }
    
    private func getCurrentDeviceName(identifire: String) {
        var userData =  UserData()
        userData.identifire = identifire
        userData.save()
    }
}
