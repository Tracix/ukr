//
//  CoreDataManager+ProximityEvent.swift
//  Tracix
//
//  Created by Aleksandr Lutenko on 10.11.2020.
//

import UIKit
import Foundation
import CoreData

extension CoreDataManager {
    
    func fetchProximities() {
        let managedContext = persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProximityEvent")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try managedContext.fetch(request)
            for data in result as! [NSManagedObject] {
               print(data.value(forKey: "peStartTime") as! String)
            }
        } catch {
            print("Failed")
        }
    }
    
    func saveProximityEvent(with device: Device) {
        let managedContext = persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "ProximityEvent", in: managedContext)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
        let dateString = dateFormatter.string(from: Date())

//        NSDate().timeIntervalSince1970
        if(device.rssi >= Constants.rssiThreshold) {
            // Add new only if it meets minimum RSSI threshold
            let newProximityEvent = NSManagedObject(entity: entity, insertInto: managedContext)
            newProximityEvent.setValue(Constants.appVersion, forKey: "appVersion")
            newProximityEvent.setValue(UIDevice.current.identifierForVendor?.uuidString, forKey: "hostDeviceIdentifier")
            newProximityEvent.setValue(device.peripheral.identifier.uuidString, forKey: "neighborDeviceIdentifier")
            newProximityEvent.setValue(dateString, forKey: "peStartTime")
            newProximityEvent.setValue(dateString, forKey: "peStopTime")
            newProximityEvent.setValue(NSDate(), forKey: "lastDetectedTimestamp")
            newProximityEvent.setValue(device.rssi, forKey: "maxRSSI")
            newProximityEvent.setValue(0, forKey: "cumulativeTimeAbovePEThresholdRSSI")
            newProximityEvent.setValue("32", forKey: "eventCode")
            do {
                try managedContext.save()
                print("Saved a new PE for \(device.peripheral.identifier): \(newProximityEvent)")
            } catch let error as NSError {
                print("Could not save: \(error), \(error.userInfo)")
            }
        }
    }
    
    func updateProximityEvent(with device: Device) {
        let managedContext = persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProximityEvent")
        request.predicate = NSPredicate(format: "neighboringDevicePeripheralIdentifier = %@",argumentArray: [device.peripheral.identifier])
        request.returnsObjectsAsFaults = false
        do {
            let result = try managedContext.fetch(request)
            for data in result as! [NSManagedObject] {
                let lastDetectedTimestamp = data.value(forKey: "lastDetectedTimestamp") as! Double
                let currTime = NSDate().timeIntervalSince1970
                let difference = currTime - lastDetectedTimestamp
                let timeSinceStart = currTime - (data.value(forKey: "startTimestamp") as! Double)
                if ((difference > Constants.secondsBeforeProximityEventExpires) || (timeSinceStart > Constants.maxDurationInSeconds) || (device.rssi < Constants.rssiLowerBound)) {
                    print(">>> Closing PE <<<")
                    closeProximityEvent(data: data)
                } else {
                    let currMaxRSSI = data.value(forKey: "maxRSSIDetected") as! Int
                    let currDeviceRSSI = device.rssi
                    let currentCumulativeTimeAboveThresholdRSSI = data.value(forKey: "cumulativeTimeAboveThresholdRSSI") as! Double
                    if (currDeviceRSSI > currMaxRSSI) {
                        print("maxRSSIDetected: \(currDeviceRSSI)")
                        data.setValue(currDeviceRSSI, forKey: "maxRSSIDetected")
                    }
                    if (currDeviceRSSI >= Constants.rssiThreshold) {
                        data.setValue(currentCumulativeTimeAboveThresholdRSSI + difference, forKey: "cumulativeTimeAboveThresholdRSSI")
                    }
                    data.setValue(NSDate().timeIntervalSince1970, forKey: "lastDetectedTimestamp")
                    do {
                        try managedContext.save()
                    } catch let error as NSError {
                        print("Could not save: \(error), \(error.userInfo)")
                    }
                }
            }
        } catch {
            print("Error fetching results.")
        }
    }
    
    func closeProximityEvent(data: NSManagedObject) {
        let peripheralID = data.value(forKey: "neighboringDevicePeripheralIdentifier") as! UUID
        let lastDetectedTimestampDouble = data.value(forKey: "lastDetectedTimestamp") as! Double
        let startTimestampDouble = data.value(forKey: "startTimestamp") as! Double
        
        // DOTE: We do not currently add the 5-sec delay timer to the end of the PE. We don't add 5 seconds.
        
//        let mutationInput = CreateProximityEventInput(appVersion: data.value(forKey: "appVersion") as! String,
//                                            cumulativeTimeAboveThresholdRssi: data.value(forKey: "cumulativeTimeAboveThresholdRSSI") as! Double,
//                                            cumulativeTimeAtMaxRssi: data.value(forKey: "cumulativeTimeAtMaxRSSI") as! Double,
//                                            hostDevicePeripheralIdentifier: data.value(forKey: "hostDevicePeripheralIdentifier") as! String,
//                                            lastDetectedTimestamp: data.value(forKey: "lastDetectedTimestamp") as! Double,
//                                            maxRssiDetected: data.value(forKey: "maxRSSIDetected") as! Int,
//                                            neighboringDevicePeripheralIdentifier: peripheralID.uuidString,
//                                            proximityEventId: UUID().uuidString,
//                                            startTimestamp: data.value(forKey: "startTimestamp") as! Double,
//                                            timeDuration: lastDetectedTimestampDouble - startTimestampDouble)

        /*
            self.deviceLabel.text = ">>> PROXIMITY EVENT ENDED AND SYNCED TO CLOUD <<<"
            
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext = appDelegate.persistentContainer.viewContext
            
            managedContext.delete(data)
            do {
                try managedContext.save()
                print("Mutation complete and PE removed from Core Data")
            } catch let error as NSError {
                print("Could not save: \(error), \(error.userInfo)")
            }
        }  */
    }
    
    func saveSmallProximityEvent(with device: Device) {
        
    }
}

//ProximityEvent:
//0) - "eventDescription" - String
//(a) - "appVersion" - String
//(b) = "hostDeviceIdentifire" - String
//(c) = "neighborDeviceIdentifire" - String
//(d) = "peStopTime" - String
//(e) = "peStartTime" - String
//(f) = "peDuration" - Double
//(g) = "maxRSSI" - Int
//(h) = "cumulativeTimeAbovePEThresholdRSSI" = Double
//
//Registration:
//0) - "eventDescription" - String
//1) = "userName" - String
//2) = "phoneNumber" - String
//3) = "adressWiFi" - String
//4) = "adressBluetooth" - String
//5) - "timeRegistration" - String
//
//RSSI Monitoring:
//(i) = "maxRSSI" - Int
//(ii) = "maxRSSIDuration" - Double
//(iii) - "eventStart" - String
//(iiii) - "eventDescription" - String
//
//Log Records:
//(x) - "eventDescription" - String
//(a) Registration; = "registration"
//(b) = "startDetectionProximityEvent" - String
//(c) = "stomDetectionProximityEvent" - String
//(d) Deregistration. = "deregistration"
