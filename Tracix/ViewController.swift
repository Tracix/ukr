//
//  ViewController.swift
//  Tracix
//
//  Created by Aleksandr Lutenko on 26.10.2020.
//

import UIKit
import CoreBluetooth
import BackgroundTasks
//import CoreLocation

class ViewController: UIViewController, SensorDelegate {
    
    @IBOutlet weak var udidLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    var peripheralManager: BluetoothPeripheralManager?
    var centralManager: BluetoothCentralService?

    var locationManager: LocationManager?
    
    var deviceIdentifier: String!
    var sensor: SensorArray?
    
    var visibleDevices = Array<Device>()
    var cachedPeripheralIdentifier = Array<String>()
    var payloadDataSupplier: PayloadDataSupplier?

    override func viewDidLoad() {
        super.viewDidLoad()
        numberLabel.text = deviceIdentifier
        udidLabel.text = "-"
        
        
        cx19OrNot(CX19: true) //if this value = true you have CX19 app. If this value = false you have our app
        
        
//        CoreDataManager.shared.fetchProximities()
    }
    
    private func cx19OrNot(CX19: Bool) {
        if CX19 {
            cx19()
        } else {
            notCX19()
        }
    }
    
    private func cx19() {
        payloadDataSupplier = MockSonarPayloadSupplier(identifier: identifier())
        sensor = SensorArray(payloadDataSupplier!)
        sensor?.add(delegate: self)
        sensor?.start()
    }
    
    private func notCX19() {
        locationManager = LocationManager()
        peripheralManager = BluetoothPeripheralManager()
        centralManager = BluetoothCentralService()
        locationManager!.start()
    }
    
    private func identifier() -> Int32 {
        let text = UIDevice.current.name + ":" + UIDevice.current.model + ":" + UIDevice.current.systemName + ":" + UIDevice.current.systemVersion
        var hash = UInt64 (5381)
        let buf = [UInt8](text.utf8)
        for b in buf {
            hash = 127 * (hash & 0x00ffffffffffffff) + UInt64(b)
        }
        let value = Int32(hash.remainderReportingOverflow(dividingBy: UInt64(Int32.max)).partialValue)
        return value
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("View controller is active")
        
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        appDelegate.monitor.pathUpdateHandler = { path in
            if path.usesInterfaceType(.wifi) {
                print("Device is using wifi, let it proceed.")
            } else {
                // TODO: What do we do if not on wireless?
                print("Device is NOT USING WIFI -- what do we do here?")
            }
        }
//        let queue = DispatchQueue(label: "Monitor")
//        appDelegate.monitor.start(queue: queue)
//
//        let managedContext = appDelegate.persistentContainer.viewContext
//        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ProximityEvent")
//
//        do {
//            proximityEvents = try managedContext.fetch(fetchRequest)
//        } catch let error as NSError {
//            print("Could not fetch \(error), \(error.userInfo)")
//        }
    }
    
    func addOrUpdatePeripheralList(device: Device) {
//        if !visibleDevices.contains(where: { $0.peripheral.identifier == device.peripheral.identifier }) {
            visibleDevices.append(device)
            saveNewProximityEvent(device: device)
//        }
//        else  {
//            for index in 0..<visibleDevices.count {
//                if (visibleDevices[index].peripheral.identifier == device.peripheral.identifier) {
//                    updateProximityEvent(device: device)
//                    break
//                }
//            }
//        }
    }
    
    func saveNewProximityEvent(device: Device) {
        CoreDataManager.shared.saveProximityEvent(with: device)
    }
    
    func updateProximityEvent(device: Device) {
        guard let deviceWillUpdate = visibleDevices.filter({ $0.identirire == device.identirire }).first else {
            return
        }
        if deviceWillUpdate.rssi < device.rssi {
            
        }
    }
}
