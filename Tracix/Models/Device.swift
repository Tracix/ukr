//
//  Device.swift
//  Tracix
//
//  Created by Aleksandr Lutenko on 29.10.2020.
//

import Foundation
import CoreBluetooth

struct Device {
    
    var peripheral: CBPeripheral
    var identirire: String
    var rssi: Int
    var deviceLastDetectedByCBManager: Double
    
    init(peripheral: CBPeripheral, identirire:String, rssi:Int) {
        self.peripheral = peripheral
        self.identirire = identirire
        self.rssi = rssi
        self.deviceLastDetectedByCBManager = NSDate().timeIntervalSince1970
    }
}
