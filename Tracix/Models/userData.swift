//
//  userData.swift
//  Tracix
//
//  Created by Aleksandr Lutenko on 29.10.2020.
//

import Foundation

struct UserData {
    
    private let userDataKey = "userData"
    
    var identifire: String = ""
    
    public init(){
        if let dictionary = UserDefaults.standard.dictionary(forKey: userDataKey) {
            identifire = dictionary["identifire"] as? String ?? ""
        }
    }
    
    public func save() {
        var dictionary : Dictionary = Dictionary<String, Any>()
        dictionary["identifire"] = identifire
        
        UserDefaults.standard.set(dictionary, forKey: userDataKey)
    }
    
}
