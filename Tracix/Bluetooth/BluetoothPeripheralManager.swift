//
//  BluetoothManager.swift
//  Tracix
//
//  Created by Aleksandr Lutenko on 16.11.2020.
//

import Foundation
import CoreBluetooth
import UIKit

class BluetoothPeripheralManager: NSObject, CBPeripheralManagerDelegate {
    
    var peripheralManager: CBPeripheralManager!
    var bluetoothServices: CBMutableService?
    var nameCharacteristic: CBMutableCharacteristic?
    var writebleCharacteristic: CBMutableCharacteristic?
    
    override init() {
        super.init()
        peripheralManager = CBPeripheralManager(delegate: self, queue: DispatchQueue.global())
        bluetoothServices = CBMutableService(type: Constants.SERVICE_UUID, primary: true)
    }
    
    func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
//        print("Peripheral: \(peripheral)")
        if (error != nil) {
            print("peripheralManagerDidStartAdvertising encountered an error.")
            print(error!.localizedDescription)  // Error: One or more parameters were invalid
        }
        print ("Debug: peripheralManagerDidStartAdvertising()")
    }
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        if (peripheral.state == .poweredOn){
            start()
        }
    }
    
    public func start() {
        configureIdentity()
    }
    
    private func configureIdentity() {
        var characteristicsArray:[CBCharacteristic] = []
        let udidDevice: Data = UIDevice.current.identifierForVendor!.uuidString.data(using: .utf8)!
        writebleCharacteristic = CBMutableCharacteristic(type: Constants.writebleCharatteristic_UUID,
                                                             properties: .write,
                                                             value: nil,
                                                             permissions: .writeable)
        nameCharacteristic = CBMutableCharacteristic(type: Constants.nameCharatteristic_UUID,
                                                     properties: (CBCharacteristicProperties.read),
                                                     value: udidDevice,
                                                     permissions: CBAttributePermissions.readable)

        characteristicsArray.append(nameCharacteristic!)
        characteristicsArray.append(writebleCharacteristic!)
        
        // more characteristics built here and added to the characteristicsArray[]...
        // ... then all are added to the CBMutableService at the bottom...
        bluetoothServices?.characteristics = characteristicsArray as [CBCharacteristic]
        peripheralManager.removeAllServices()
        publishServices(newService: bluetoothServices)
    }
    
    
    private func publishServices(newService:CBMutableService!) {
        peripheralManager.add(newService)
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error: Error?) {
        if (error != nil) {
            print("PerformerUtility.publishServices() returned error: \(error!.localizedDescription)")
        }
        else {
            updateAdvertisingData(service: service)
        }
    }
    
    func updateAdvertisingData(service: CBService) {
        if (peripheralManager.isAdvertising) {
            peripheralManager.stopAdvertising()
        }
        let udidDevice: String = UIDevice.current.identifierForVendor!.uuidString
        peripheralManager.startAdvertising([CBAdvertisementDataServiceUUIDsKey: [service.uuid], CBAdvertisementDataLocalNameKey: udidDevice])
        print("startAdvertising: \(service)")
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest) {
        print("didReceiveRead: \(request)")
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]) {
        print("didReceiveWrite: \(requests)")
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, willRestoreState dict: [String : Any]) {
        print("willRestoreState")
    }
}
