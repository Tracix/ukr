//
//  BluetoothCentralService.swift
//  Tracix
//
//  Created by Aleksandr Lutenko on 21.11.2020.
//

import UIKit
import CoreBluetooth

class BluetoothCentralService: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var centralManager: CBCentralManager?
    private var bluetoothQueue: DispatchQueue
//    private var scanTimer: DispatchSourceTimer?
//    private let scanTimerQueue = DispatchQueue(label: "Sensor.BLE.ConcreteBLEReceiver.ScanTimer")
//    private let scheduleScanQueue = DispatchQueue(label: "Sensor.BLE.ConcreteBLEReceiver.ScheduleScan")
    private var connectedPeripheral: CBPeripheral?
    
    override init() {
        bluetoothQueue = DispatchQueue(label: "com.tracix.centralqueue", qos: .background, attributes: .concurrent, autoreleaseFrequency: .workItem, target: nil)
        super.init()
        centralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
    }
    
//    public func scheduleScan() {
//        scheduleScanQueue.sync {
//            scanTimer?.cancel()
//            scanTimer = DispatchSource.makeTimerSource(queue: scanTimerQueue)
//            scanTimer?.schedule(deadline: DispatchTime.now() + BLESensorConfiguration.notificationDelay)
//            scanTimer?.setEventHandler { [weak self] in
//                self?.starScan()
//            }
//            scanTimer?.resume()
//        }
//    }
    
    public func starScan() {
        stopScanning()
//        self.bluetoothQueue.async {
            self.centralManager!.scanForPeripherals(withServices: [Constants.SERVICE_UUID], options: [CBCentralManagerScanOptionAllowDuplicatesKey : true])
            print("startscanForPeripherals: \(Constants.SERVICE_UUID)")
//        }
    }
    
    private func stopScanning() {
        print("\(Date()): Stopping scan");
        self.centralManager?.stopScan()
    }
    
    func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
        print("willRestoreState")
        starScan()
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("centralManagerDidUpdateState \(central.state)")
        switch central.state {
            case .poweredOff:
                print("Update state (state=poweredOff)")
            case .poweredOn:
                print("Update state (state=poweredOn)")
                starScan()
            case .resetting:
                print("Update state (state=resetting)")
            case .unauthorized:
                print("Update state (state=unauthorized)")
            case .unknown:
                print("Update state (state=unknown)")
            case .unsupported:
                print("Update state (state=unsupported)")
            default:
                print("Update state (state=undefined)")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("didDiscover peripheral")
        guard connectedPeripheral == nil else { return }
        connectedPeripheral = peripheral
        peripheral.delegate = self
        central.connect(peripheral, options: nil)
//        stopScanning()
//        starScan()
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("didConnect peripheral")
        peripheral.discoverServices([Constants.SERVICE_UUID])
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("didDisconnectPeripheral")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        print("didReadRSSI \(RSSI.intValue)")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        print("didDiscoverServices")
        guard let service = peripheral.services?.filter({ $0.uuid == Constants.SERVICE_UUID }).first else {
            connectedPeripheral = nil
            return
        }
        peripheral.discoverCharacteristics([Constants.nameCharatteristic_UUID], for: service)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        print("didDiscoverCharacteristicsFor")
        guard let characteristic = service.characteristics?.filter({ $0.uuid == Constants.nameCharatteristic_UUID }).first else {
            connectedPeripheral = nil
            return
        }
        
        peripheral.readValue(for: characteristic)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("didWriteValueFor")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print("didUpdateValueFor")
        guard characteristic.uuid == Constants.nameCharatteristic_UUID, let value = characteristic.value else {
            connectedPeripheral = nil
            return
        }
        
        print("Value = \(String(describing: String(data: value, encoding: .utf8)))")
        connectedPeripheral = nil
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
        print("didDiscoverDescriptorsForCharacteristic")
    }
}

//let phoneIndentifire: String = advertisementData[CBAdvertisementDataLocalNameKey] as? String ?? "Unnown"
//if phoneIndentifire != "Unnown" {
////            DispatchQueue.main.async {
////                self.udidLabel.text = "\(advertisementData[CBAdvertisementDataLocalNameKey] as? String ?? "Unnown"): \(RSSI)"
////            }
//    guard let advertisementIdentifire: String = advertisementData[CBAdvertisementDataLocalNameKey] as? String else {
//        return
//    }
//    let device = Device(peripheral: peripheral, identirire: advertisementIdentifire, rssi: RSSI.intValue)
////                addOrUpdatePeripheralList(device: device)
////                print("\(String(describing: advertisementData[CBAdvertisementDataLocalNameKey])): \(RSSI)")
//} else {
//    if let phoneIndentifireOverfloe: String = advertisementData[CBAdvertisementDataLocalNameKey] as? String {
//        print("CBAdvertisementDataLocalNameKey: \(phoneIndentifireOverfloe)")
//    }
//    if RSSI.intValue < -50 {
//        if (advertisementData[CBAdvertisementDataOverflowServiceUUIDsKey] != nil) {
//            if let overflowIds = advertisementData[CBAdvertisementDataOverflowServiceUUIDsKey] {
//                if let overflowIds = overflowIds as? [CBUUID] {
//                    print(overflowIds)
//                }
//            }
//        }
//    }
//
//    let device = Device(peripheral: peripheral, identirire: peripheral.identifier.uuidString, rssi: RSSI.intValue)
////                addOrUpdatePeripheralList(device: device)
////            print("\(Date()) :\(phoneIndentifire) - \(RSSI)")
////            DispatchQueue.main.async {
////                self.udidLabel.text = "\(phoneIndentifire) - \(RSSI)"
////            }
//}
